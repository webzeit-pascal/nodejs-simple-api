// Swagger set up
const options = {
    swaggerDefinition: {
      openapi: "3.0.0",
      info: {
        title: "Simple quiz API",
        version: "1.0.0",
        description:
          "Test project for candidates",
        license: {}
      },
      servers: [
        {
          url: "http://localhost:3000"
        }
      ]
    },
    apis: [
        "./index.js",
        "./.data/quiz.js"
    ]
};

module.exports = {
    options
}