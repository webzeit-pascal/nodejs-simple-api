const express = require('express')
var bodyParser = require('body-parser')

const app = express()
app.use(bodyParser.json({ type: 'application/json' }))

const port = 3000

const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('./.data/db.json')
const db = low(adapter)

db.defaults({ questions: [] })
  .write()

/**
 * @swagger
 * path:
 *  /questions:
 *    get:
 *      summary: List of all quiz questions
 *      responses:
 *        "200":
 *          description: Question schema
 *          content:
 *            application/json:
 *              schema:
*                 $ref: '#/components/schemas/Question'
 *                  
 */
app.get('/questions', (req, res) => {
    let quiz = require('./.data/quiz.js')
    res.send(quiz)
})

/**
 * @swagger
 * path:
 *  /store:
 *    post:
 *      summary: Store object
 *      requestBody:
 *        description: Any object definition is acceptable, object that will be stored are defined by consumer
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              example: {id: 1, "answer": "This is me"}
 *      responses:
 *        "200":
 *          content:
 *            application/json:
 *              schema:
 *                 properties:
 *                    status:
 *                      type: boolean             
 *                  
 */
app.post('/store', (req, res) => {
    // Add a post
    let items = db.get('questions')
    .filter(req.body)
    .value()

    if (items.length > 0) {
        return res.status(400).send({
            status: false,
            message: "Same model already exist"
        })
    }
    // Add a post
    db.get('questions')
    .push(req.body)
    .write()

    res.send({status: true})
})

/**
 * @swagger
 * path:
 *  /find:
 *    post:
 *      summary: Find object by filters
 *      requestBody:
 *        description: Find will search for objects defined in filters, all objects matching will be returned
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              example: {id: 1}
 *      responses:
 *        "200":
 *          content:
 *            application/json:
 *              schema:
 *                 properties:
 *                    status:
 *                      type: boolean
 * 
 
 *                  
 *                  
 */
app.post('/find', (req, res) => {
    // Add a post
    let items = db.get('questions')
    .filter(req.body)
    .value()

    res.send(items)
})

/**
 * @swagger
 * path:
 *  /update:
 *    post:
 *      summary: Update object by filter
 *      requestBody:
 *        description: Objects that match filter will be updated with definition from model
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              example: {filters: {id: 1}, model: {answer: "All objects matching filters will be updated with this"}}
 *      responses:
 *        "200":
 *          content:
 *            application/json:
 *              schema:
 *                 properties:
 *                    status:
 *                      type: boolean
 * 
 *        "400":
 *          content:
 *            application/json:
 *              schema:
 *                 properties:
 *                    status:
 *                      type: boolean
 *                    message:
 *                      type: string
 *                  
 *                  
 */
app.post('/update', (req, res) => {
    // Add a post
    let items = db.get('questions')
    .filter(req.body.filters)
    .value()

    if (items.length < 1) {
        return res.status(400).send({
            status: false,
            message: "no record found"
        })
    }

    db.get('questions')
    .find(req.body.filters)
    .assign(req.body.model)
    .write()

    return res.send({status: true})
})

/**
 * @swagger
 * path:
 *  /remove:
 *    post:
 *      summary: Remove object by filter
 *      requestBody:
 *        description: Objects that match filter will be removed
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              example: {filters: {id: 1}, model: {answer: "All objects matching filters will be updated with this"}}
 *      responses:
 *        "200":
 *          content:
 *            application/json:
 *              schema:
 *                 properties:
 *                    status:
 *                      type: boolean
 *                  
 *                  
 */
app.post('/remove', (req, res) => {
    // Add a post
    db.get('questions')
    .remove(req.body)
    .write()

    res.send({status: true})
})

/**
 * @swagger
 * path:
 *  /clean:
 *    post:
 *      summary: Clean storage
 *      responses:
 *        "200":
 *          content:
 *            application/json:
 *              schema:
 *                 properties:
 *                    status:
 *                      type: boolean
 *         
 */
app.post('/clean', (req, res) => {
    // Add a post
    db.get('questions')
    .remove({})
    .write()

    res.send({status: true})
})



const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const {options} = require('./docs.config')
const specs = swaggerJsdoc(options);
app.use("/docs", swaggerUi.serve);
app.get(
  "/docs",
  swaggerUi.setup(specs, {
    explorer: true
  })
);

app.listen(port, () => console.log(`Listening on port ${port}!`))