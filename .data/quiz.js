/**
 * @swagger
 *  components:
 *    schemas:
 *      Question:
 *        type: object
 *        properties:
 *          id:
 *            type: integer
 *          question:
 *            type: string
 *          input:
 *            oneOf:
 *               - $ref: '#/components/schemas/InputText'
 *               - $ref: '#/components/schemas/InputRadio'
 *               - $ref: '#/components/schemas/InputCheckbox'
 *               - $ref: '#/components/schemas/InputTextarea'
 *        example: 
 *            [{
 *                   "id": 1,
 *                   "question": "What is your name?",
 *                   "input": {
 *                       "type": "text",
 *                       "default": null,
 *                       "constraints": {
 *                           "required": true,
 *                           "min": 2,
 *                           "max": 25
 *                       }
 *                   }
 *               }]
 */
module.exports = [
    {
        "id": 1,
        "question": "What is your name?",
        "input": {
            /**
             * @swagger
             *  components:
             *    schemas:
             *      InputText:
             *        type: object
             *        properties:
             *          type:
             *            type: string
             *            default: text
             *          default:
             *            type: string
             *            nullable: true
             *          constraints:
             *            type: object
             *            properties:
             *              required:
             *                type: boolean
             *              min:
             *                type: integer
             *              max:
             *                type: integer
             */
            "type": "text",
            "default": null,
            "constraints": {
                "required": true,
                "min": 2,
                "max": 25
            }
        }
    },
    {
        "id": 2,
        "question": "How you feel today?",
        "input": {
            /**
             * @swagger
             *  components:
             *    schemas:
             *      InputRadio:
             *        type: object
             *        properties:
             *          type:
             *            type: string
             *            default: radio
             *          default:
             *            type: string
             *            nullable: true
             *          values:
             *            type: array
             *            items:
             *              type: string
             *          constraints:
             *            type: object
             *            properties:
             *              required:
             *                type: boolean
             */
            "type": "radio",
            "default": "good",
            "values": ["good","bad"],
            "constraints": {
                "required": true
            }
        }
    },
    {
        "id": 3,
        "question": "Select words that are most likable to you (min 2 max 5)",
        "input": {
            /**
             * @swagger
             *  components:
             *    schemas:
             *      InputCheckbox:
             *        type: object
             *        properties:
             *          type:
             *            type: string
             *            default: radio
             *          default:
             *            type: array
             *            nullable: true
             *            items: 
             *              type: string
             *          values:
             *            type: array
             *            items:
             *              type: string
             *          constraints:
             *            type: object
             *            properties:
             *              required:
             *                type: boolean
             */
            "type": "checkbox",
            "default": ["beer", "responsibility"],
            "values": [
                "enthusiasm",
                "leadership",
                "industry",
                "loss",
                "sympathy",
                "emotion",
                "consequence",
                "beer",
                "energy",
                "responsibility"
            ],
            "constraints": {
                "required": true
            }
        }
    },
    {
        "id": 4,
        "question": "Describe self in few words...",
        "input": {
            /**
             * @swagger
             *  components:
             *    schemas:
             *      InputTextarea:
             *        type: object
             *        properties:
             *          type:
             *            type: string
             *            default: radio
             *          default:
             *            type: array
             *            nullable: true
             *            items: 
             *              type: string
             *          values:
             *            type: array
             *            items:
             *              type: string
             *          constraints:
             *            type: object
             *            properties:
             *              required:
             *                type: boolean
             */
            "type": "textarea",
            "default": null,
            "constraints": {
                "required": false,
                "min": 5,
                "max": 255,
                "forbidenWords": []
            }
        }
    }
]